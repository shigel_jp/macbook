Macbook
---------

### 概要

Macbookのshellはここに置く。

### Homebrewのインストール

パスワードを入力が要求されたら、Macログイン時のパスワードを入力

```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

mas-cliのインストール

```
brew install mas
```

### ソフトウェアアップデート

```
curl https://bitbucket.org/shigel_jp/macbook/raw/9cfdf1d25623608671f87a4d45024f770872d359/bin/struct.sh | sh -x
```

### OSアップデート

https://support.apple.com/ja-jp/HT200113

```
sudo -s
softwareupdate --list
softwareupdate --install "Command Line Tools beta 5 for Xcode-12.0"
softwareupdate --install "macOS Catalina 10.15.6 Update-"
```
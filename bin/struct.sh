#!/bin/sh

brew cask install vagrant
brew cask install virtualbox
brew cask install sourcetree
brew cask install google-chrome
#brew cask install ansible
#brew cask install git
#brew cask install git-flow
#brew cask install mysql
#brew cask install openssh
#brew cask install openssl
#brew cask install python
#brew cask install ssh-copy-id
#brew cask install sshpass
#brew cask install wget
brew cask install wireshark
brew cask install google-japanese-ime
brew cask install google-backup-and-sync
brew cask install google-drive-file-stream
brew cask install google-chrome-canary

#$ mas list
#mas install 715768417 # Microsoft Remote Desktop (8.0.27246)
#mas install 478570084 # CompareMerge (2.00)
mas install 497799835 # Xcode (8.2.1)
#mas install 412448059 # ForkLift (2.6.6)
mas install 539883307 # LINE (4.12.1)
#mas install 425424353 # The Unarchiver (3.11.1)
mas install 803453959 # Slack (4.8.0)
mas install 405399194 # Kindle (1.29.0)
